-module(db_test).

-include_lib("eunit/include/eunit.hrl").

-compile(export_all).

success_test() ->
    Db = "test_db",
    db:stop(Db),
    RecordKey = 2,
    Record = {RecordKey, "MyUser", "MyCity"},
    RecordUpdated = {RecordKey, "YourUser", "YourCity"},
    ?assertEqual(ok, db:new(Db)),
    ?assertEqual({ok, Record}, db:create(Record, Db)),
    ?assertEqual({ok, Record}, db:read(RecordKey, Db)),
    ?assertEqual({ok, RecordUpdated}, db:update(RecordUpdated, Db)),
    ?assertEqual({ok, RecordUpdated}, db:read(RecordKey, Db)),
    ?assertEqual(ok, db:delete(RecordKey, Db)),
    ?assertEqual({error, not_found}, db:read(RecordKey, Db)),
    ?assertEqual(ok, db:stop(Db)).

db_already_exist_test() ->
    ?assertEqual(ok, db:new("TestDb")),
    ?assertEqual({error, db_already_exist}, db:new("TestDb")),
    ?assertEqual(ok, db:stop("TestDb")).

db_not_exist_test() -> 
    Record = {2, "MyUser", "MyCity"},
    Db = "not_exist_db",
    ?assertEqual({error, db_not_exist}, db:create(Record, Db)),
    ?assertEqual({error, db_not_exist}, db:read(2, Db)),
    ?assertEqual({error, db_not_exist}, db:update(Record, Db)),
    ?assertEqual({error, db_not_exist}, db:delete(2, Db)).

record_not_exist_test() ->
    RecordKey = 2,
    Record = {RecordKey, "MyUser", "MyCity"},
    Db = "my_db",
    ?assertEqual(ok, db:new(Db)),
    ?assertEqual({error, not_found}, db:read(RecordKey, Db)),
    ?assertEqual({error, not_found}, db:update(Record, Db)),
    ?assertEqual({error, not_found}, db:delete(RecordKey, Db)),
    ?assertEqual(ok, db:stop(Db)).

create_already_exist_test() ->
    Db = "test_db",
    RecordKey = 2,
    Record = {RecordKey, "MyUser", "MyCity"},
    ?assertEqual(ok, db:new(Db)),
    ?assertEqual({ok, Record}, db:create(Record, Db)),
    ?assertEqual({error, already_exist}, db:create(Record, Db)),
    ?assertEqual(ok, db:stop(Db)).