%%%-------------------------------------------------------------------
%%% @author Artem Golovinsky <artemgolovinsky@gmail.com>
%%% @copyright (C) 2019
%%% @doc
%%%    Word counter in files.
%%%    Limitations for input files:
%%%    1. All words should begin with lowercase character.
%%%    2. File can contain composite words with dash like "близко-близко"
%%%    3. Part of word can be moved to next line using dash, line break should be placed after dash.
%%% @end
%%%-------------------------------------------------------------------
-module(map_reduce).

-behaviour(gen_server).

-export([start/1]).
-export([init/1,
		 handle_call/3,
		 handle_cast/2,
		 handle_info/2,
		 terminate/2,
		 code_change/3
		]).
-export([worker/3]).

-define(VALID_CHARS, "абвгдеёжзийклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyz-").

%%%===================================================================
%%% API
%%%===================================================================

%%% @doc Start count words in files. List of filenames should be passed.
-spec start([string()]) -> #{string() => pos_integer()}.
start(FileList) ->
    {ok, Pid} = gen_server:start_link(?MODULE, [], []),
    Result = gen_server:call(Pid, {start, FileList}, 30000),
    Result.

%%%===================================================================
%%% INTERNAL API
%%%===================================================================

%%% @doc Worker notifies controlling process using this function.
-spec count_done(pid(), #{string() => pos_integer()}) -> ok.
count_done(Parent, WordMap) -> 
    gen_server:cast(Parent, {count_done, self(), WordMap}).

%%%===================================================================
%%% gen_server callbacks and state definition
%%%===================================================================
-record(state, {
    %% result map with counters
    result_map :: [map()],
    %% map of workers. Key - pid, value - filename
    workers :: map:map(),
    %% Pid to send reply
    reply_pid :: pid()
}).

%% init gen_server callback
init([]) ->
    process_flag(trap_exit, true),
    {ok, #state{}}.

%% call callback. 
handle_call({start, FileList}, {CallerPid, _Tag} = From, _State) ->
    %% stop processing if calling process crashed. 
    link(CallerPid), 
    ValidChars = create_valid_chars(?VALID_CHARS),
    Workers =
        lists:foldl(fun(File, MapAcc) -> 
            Pid = spawn_link(?MODULE, worker, [self(), File, ValidChars]),
            maps:put(Pid, File, MapAcc)
        end, #{}, FileList),
    {noreply, #state{
        workers = Workers, 
        reply_pid = From,
        result_map = #{}}}.

%% cast callback.
handle_cast({count_done, WorkerPid, WordMap}, State) ->
    ResultMap = reduce(WordMap, State#state.result_map),
    Workers = maps:remove(WorkerPid, State#state.workers),
    case maps:size(Workers) == 0 of 
        true ->
            {ReplyPid, _Tag} = From = State#state.reply_pid,
            unlink(ReplyPid),
            gen_server:reply(From, ResultMap),
            {stop, normal, undefined};
        false -> 
            {noreply, State#state{result_map = ResultMap, workers = Workers}}
    end.

%% normal finish of workers. Nothing to do
handle_info({'EXIT', _From, normal}, State) ->
    {noreply, State};

%% Worker crashed. Counters from file will not be included to final map
handle_info({'EXIT', From, {Reason, _St}}, #state{workers = Workers0} = State) ->
    Workers1 =
        case maps:find(From, Workers0) of
            {ok, Filename} ->
                error_logger:error_msg("Failed processing for file ~s with pid ~p with reason: ~p",[Filename, From, Reason]),
                maps:remove(From, Workers0);
            error ->
                error_logger:warning_msg("Error message from already finished worker. Pid ~p reason: ~p",[From, Reason]),
                Workers0
        end,
    case maps:size(Workers1) == 0 of 
        true ->
            gen_server:reply(State#state.reply_pid, State#state.result_map),
            {stop, normal, undefined};
        false -> 
            {noreply, State#state{workers = Workers1}}
    end.

%% terminate callback
terminate(_Reason, _State) ->
    ok.
%% code change callback. NOT USED
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%%% WORKER COUNT WORDS %%%
worker(Parent, File, ValidChars) ->
    error_logger:info_msg("Start processing file ~s with pid ~p", [File, self()]),
    WordMap = do_count(File, ValidChars),
    error_logger:info_msg("Successfully processed file ~s with pid ~p", [File, self()]),
    count_done(Parent, WordMap).

do_count(File, ValidChars) ->
    case file:open(File, [read, {encoding, utf8}]) of 
        {ok, Fd} -> count_words(Fd, ValidChars, #{});
        {error, Reason} -> error(Reason)
    end.

%% Conditions for valid file
%% 1. Each word is valid
%% 2. Valid word can contatin lowercase characters from russian and english alphabet and dash
%% 3. Dash is used to move word to next line. 
count_words(Fd, ValidChars, WordMap) -> 
    count_words(Fd, ValidChars, WordMap, []). 

count_words(Fd, ValidChars, WordMap, CurrentWord) -> 
    case io:get_line(Fd, "") of
        eof -> 
            WordMap;
        Data -> 
            {NewMap, NewCurrentWord} = count_from_line(WordMap, Data, CurrentWord, ValidChars),
            count_words(Fd, ValidChars, NewMap, NewCurrentWord)
    end.

count_from_line(WordMap, [$-,10], CurrentWord, _ValidChars) ->
    {WordMap, CurrentWord};
count_from_line(WordMap, [], [], _ValidChars) ->
    {WordMap, []};
count_from_line(WordMap, [], CurrentWord, _ValidChars) -> 
    NewMap = add_to_map(WordMap, lists:reverse(CurrentWord)),
    {NewMap, []};
count_from_line(WordMap, [Ch|Data], CurrentWord, ValidChars) -> 
    case is_word_char(Ch, ValidChars) of 
        true ->
            count_from_line(WordMap, Data, [Ch|CurrentWord], ValidChars);
        false ->
            NewMap = add_to_map(WordMap, lists:reverse(CurrentWord)),
            count_from_line(NewMap, Data, [], ValidChars)
    end.

add_to_map(WordMap, []) ->
    WordMap;
add_to_map(WordMap, CurrentWord) ->
    case maps:find(CurrentWord, WordMap) of
        {ok, Count} -> maps:put(CurrentWord, Count + 1, WordMap);
        error -> maps:put(CurrentWord, 1, WordMap)
    end.
%%% WORKER COUNT WORDS ENDS %%%

%%% REDUCE MAP TO FINAL MAP %%%
reduce(WordMap, FinalMap) ->
    Iter = maps:iterator(WordMap),
    reduce_iter(Iter, FinalMap).

reduce_iter(none, FinalMap) ->
    FinalMap;
reduce_iter(Iter, FinalMap) -> 
    {W, Count, NextIter} = maps:next(Iter),
    NewCount = 
        case maps:find(W, FinalMap) of 
            error -> Count;
            {ok, Value} -> Count + Value
        end,
    NewFinalMap = maps:put(W, NewCount, FinalMap),
    reduce_iter(NextIter, NewFinalMap).
%%% END REDUCE MAP TO FINAL MAP %%%

create_valid_chars(CharsList) ->
    lists:foldl(fun(C, CharMap) -> maps:put(C, undefined, CharMap) end, #{}, CharsList).

is_word_char(C, ValidChars) ->
    maps:is_key(C, ValidChars).





