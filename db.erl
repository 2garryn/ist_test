%%%-------------------------------------------------------------------
%%% @author Artem Golovinsky <artemgolovinsky@gmail.com>
%%% @copyright (C) 2019
%%% @doc
%%%     CRUD Database emulation based on ETS.
%%% @end
%%%-------------------------------------------------------------------
-module(db).

-export([
    new/1,
    create/2,
    read/2,
    update/2,
    delete/2,
    stop/1
]).
-export([create_db/2]).

-define(PROC_PREFIX, "_db_").
-define(CREATE_DB_TIMEOUT, 5000).


-type db_name() :: string().
-type key() :: non_neg_integer().
-type username() :: string().
-type city() :: string().

-type data_record() :: {key(), username(), city()}.

-record(location, {
    key :: key(),
    username :: username(),
    city :: city()
}).


%%%===================================================================
%%% API
%%%===================================================================

%%% @doc Create new database.
-spec new(db_name()) -> ok | {error, db_already_exist}.

new(DbName) -> 
    do_new(DbName).

%%% @doc Create new record in database.
-spec create(data_record(), db_name()) -> 
    {ok, data_record()} | {error, db_not_exist} | {error, already_exist}.

create(Rec0, DbName) ->
    Rec1 = rec_to_location(Rec0),
    case execute(DbName, insert_new, [Rec1]) of 
        {error, db_not_exist} = Error -> Error;
        true -> {ok, location_to_rec(Rec1)};
        false -> {error, already_exist}
    end.

%%% @doc Read record from database by key
-spec read(key(), db_name()) -> 
    {ok, data_record()} | {error, db_not_exist} | {error, not_found}.

read(Key, DbName) ->
    case execute(DbName, lookup, [Key]) of 
        {error, db_not_exist} = Error -> Error;
        [] -> {error, not_found};
        [Rec] -> {ok, location_to_rec(Rec)}
    end.

%%% @doc Update record in database
-spec update(data_record(), db_name()) -> 
    {ok, data_record()} | {error, db_not_exist} | {error, not_found}.

update(Rec0, DbName) ->
    Rec1 = rec_to_location(Rec0),
    Key = Rec1#location.key,
    ElSpec = [
        {#location.username, Rec1#location.username},
        {#location.city, Rec1#location.city}
    ],
    case execute(DbName, update_element, [Key, ElSpec]) of
        {error, db_not_exist} = Error -> Error;
        true -> {ok, location_to_rec(Rec1)};
        false -> {error, not_found}
    end.

%%% @doc Delete record from database ny key
-spec delete(key(), db_name()) -> 
    ok | {error, db_not_exist} | {error, not_found}.

delete(Key, DbName) ->
    MatchSpec = [{#location{key = Key, _ = '_'}, [], [true]}],
    case execute(DbName, select_delete, [MatchSpec]) of
        {error, db_not_exist} = Error -> Error;
        0 -> {error, not_found};
        1 -> ok
    end.

%%% @doc Stop and remove database
-spec stop(db_name()) -> ok.

stop(DbName) ->
    AtomName = list_to_db_name(DbName),
    case whereis(AtomName) of 
        undefined -> ok;
        Pid -> exit(Pid, kill)
    end,
    ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================
execute(DbName, Op, Args) ->
    case is_db_exist(DbName) of
        {ok, AtomName} ->
            try
                apply(ets, Op, [AtomName|Args])
            catch _:_ ->
                {error, db_not_exist}
            end;
        error -> 
            {error, db_not_exist}
    end.

do_new(DbName) ->
    AtomName = list_to_db_name(DbName),
    Pid = spawn(?MODULE, create_db, [self(), AtomName]),
    receive
        created -> ok;
        {error, Reason} -> {error, Reason}
    after ?CREATE_DB_TIMEOUT ->
        exit(Pid, timeout),
        {error, timeout}
    end.

create_db(ReplyPid, AtomName) ->
    try 
        register(AtomName, self()),
        ets:new(AtomName, [named_table, public, {keypos, #location.key}, set]),
        ReplyPid ! created,
	    receive
	    after
		    infinity -> true
	    end
    catch _:_ ->
        ReplyPid ! {error, db_already_exist}
    end.


list_to_db_name(DbName) ->
    FullName = ?PROC_PREFIX ++ DbName,
    try
        list_to_existing_atom(FullName)
    catch _:_ ->
        list_to_atom(FullName)
    end.

is_db_exist(DbName) ->
    FullName = ?PROC_PREFIX ++ DbName,
    try
        {ok, list_to_existing_atom(FullName)}
    catch _:_ ->
        error
    end.

rec_to_location({Key, Username, City}) -> 
    #location{key = Key, username = Username, city = City}.

location_to_rec(#location{key = Key, username = Username, city = City}) ->
    {Key, Username, City}.
